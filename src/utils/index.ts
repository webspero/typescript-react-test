export const getCol = () =>{
    const columns = [
        {
          title: "Book Title",
          dataIndex: "title",
          key: "title",
        },
        {
          title: "Author Name",
          dataIndex: "author_name",
          key: "author_name",
        },
        {
          title: "Publish Year",
          dataIndex: "first_publish_year",
          key: "first_publish_year",
        },
        {
          title: "ISBN No.",
          dataIndex: "isbn",
          key: "isbn",
        },
        {
          title: "Number of pages",
          dataIndex: "number_of_pages_median",
          key: "number_of_pages_median",
        },
      ];
      return columns;
}