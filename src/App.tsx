import React, { useEffect } from "react";
import { Input, Table, Button, Col, Row } from "antd";
import { ArrowUpOutlined, ArrowDownOutlined } from "@ant-design/icons";

import axios from "axios";
import { getCol } from "./utils";
import useDebounce from "./hooks/useDebounce";
import "./App.css";

const baseURL = "https://openlibrary.org/search.json";

interface BookProps {
  title: string;
  author_name: string;
  first_publish_year: number;
  isbn: number;
  number_of_pages_median: number;
}

function App() {
  const [book, setBook] = React.useState([]);
  const [res, setRes] = React.useState(false);
  const [value, setValue] = React.useState("");
  const [sortData, setSortData] = React.useState(0);
  const [loading, setLoading] = React.useState(false);

  const debouncedCallback = useDebounce(value, 1000);

  const handleChange = (e: any) => {
    setValue(e.target.value);
  };

  useEffect(() => {
    if (debouncedCallback.length > 0) {
      setLoading(true);
      axios.get(baseURL + "?q=" + debouncedCallback).then((response) => {
        setBook(
          response.data.docs.filter((item: BookProps) => {
            return item.title
              .toLowerCase()
              .includes(debouncedCallback.toLowerCase());
          })
        );
        setLoading(false);
      });
    }
  }, [debouncedCallback]);

  useEffect(() => {
    if (book.length > 0) setRes(true);
    if (debouncedCallback.length === 0) {
      setRes(false);
      setLoading(false);
    }
  }, [book, debouncedCallback]);

  const asc = () => {
    const newBooks = [...book];
    newBooks &&
      newBooks.length > 0 &&
      newBooks.sort((a, b) => {
        return a["first_publish_year"] - b["first_publish_year"];
      });
    setSortData(1);
    setBook(newBooks);
  };

  const des = () => {
    const newBooks = [...book];
    newBooks &&
      newBooks.length > 0 &&
      newBooks.sort((a, b) => {
        return b["first_publish_year"] - a["first_publish_year"];
      });
    setSortData(0);
    setBook(newBooks);
  };

  return (
    <div className="App">
      <Row>
        <Col span={20}>
          <Input
            placeholder="Enter Book Name"
            style={{ width: "50%" }}
            onChange={handleChange}
          />
        </Col>
        <Col span={4}>
          {res ? (
            <>
              {sortData === 0 ? (
                <Button icon={<ArrowUpOutlined />} onClick={asc}>
                  Asc
                </Button>
              ) : (
                <Button icon={<ArrowDownOutlined />} onClick={des}>
                  Des
                </Button>
              )}
            </>
          ) : (
            <div></div>
          )}
        </Col>
      </Row>

      <div className="tableRes">
        {loading ? (
          <div>Loading...</div>
        ) : (
        res ?  <Table columns={getCol()} dataSource={book} pagination={false} /> : <div>No Records</div>
        )}
      </div>
    </div>
  );
}

export default App;
